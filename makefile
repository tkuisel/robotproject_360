all: server client
server: ProxyServer.c DieWithError.c
	gcc -o server ProxyServer.c DieWithError.c -Wall
client: client.c
	gcc -o client client.c -Wall
clean:
	rm -f server client
