#include <stdio.h>
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <netdb.h>      /* for getHostByName() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <stdint.h>		/* for standard datatypes */

/* Command definitions */

#define CONNECT_CMD 0
#define QUIT_CMD 255
#define IMAGE_CMD 2
#define GPS_CMD 4
#define DGPS_CMD 8
#define LASERS_CMD 16
#define MOVE_CMD 32
#define TURN_CMD 64
#define STOP_CMD 128

/* Commands can be OR together to create multiple commands in one packet 
   but, we dont need to OR them together. can just send each command seperately.

	 connect = 0000 0000
	 image   = 0000 0010
	 GPS     = 0000 0100
	 dGPS    = 0000 1000
	 lasers  = 0001 0000
	 move    = 0010 0000
	 turn    = 0100 0000
	 stop    = 1000 0000
	 quit    = 1111 1111

	 */


typedef struct buffer_t {
  int size, len;
  unsigned char* data;
} buffer;

	 /* protocol header data */
	 struct header
	 {
		 	uint32_t ID; 		// Protocol Identifier
			uint32_t pass;		// Password
			uint32_t request;	// Client Request
			uint32_t reqData;	// Request Data
			uint32_t offset;	// Byte Offset
			uint32_t totalSize;	// Total Size in Bytes
			uint32_t Paysize;	// Payload Size		
	 };
