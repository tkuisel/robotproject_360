/**
	Thomas Kuisel
	CPSC 360 Fall 2015
	HW2

	getfile.c
	A TCP WebClient that uses HTTP 
	protocol.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#include <stdbool.h>

void error(const char *message) {
	fprintf(stderr, "%s\n", message);
	exit(1);
}

void usererror(const char *message, const char *detail) {
	fprintf(stderr, "%s", message);
	fprintf(stderr, ":");
	fprintf(stderr, "%s\n", detail);
	exit(1);
}

int main( int argc, char **argv ) {	
	int portnumber;  // the port
	portnumber = 8080;  // it should always be 8080
	int file = 0; // flag variable for files
	int sock; // sock variable	
 
	char buf[200];	
	char *url; // the url
	char *buf2;
	char *hostname;
	char *servername; // name of the server
	char *getdata; // data of the host
	char *fname;  // filename
	char *http;  // url http

	FILE *output; // output file
	struct hostent *host; // the host struct
	struct sockaddr_in server; // server struct

	// check the args
	if ((argc > 7) || (argc < 2)) {
		usererror("Usage:", "<URL> [-t or -p <Port>] [-f <Filename>]");
	}
			
	// save the URL from the args
	url = argv[1];
	
	int count;
	// Interpret the args to get the filename and other info

	// parse command line arguments
	

	// allocate memory for name strings
	getdata = (char *)malloc(sizeof(url));
	servername = (char *)malloc(sizeof(url));
	
   // parse the names to ignore slashes for directories
	http = strchr(argv[1],':');	

	//Build the entire http request. based on HTTP documentation
	if ( http != NULL ) 
		servername = http + 3;
	else 
		servername = argv[1];

	strcpy(buf,servername);
	buf2 = strchr(servername, '/');

	// get host ip data
	if (buf2 == NULL) 
		hostname = servername;
	else  {
		hostname = strtok(buf, "/");
		buf2 ++;
	}			

	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if(sock < 0 )
		error("socket failed");

	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr(hostname);
	
	if (server.sin_addr.s_addr == -1) {
        	host = gethostbyname(hostname);
         server.sin_addr.s_addr = *((unsigned long *) host->h_addr_list[0]);
    }
	
	server.sin_port = htons(portnumber);

	if( connect(sock,(struct sockaddr *)&server, sizeof(server)) < 0)
		error("connect failed");
	// connect to the server

	char *request = (char *)malloc(sizeof(char));
	sprintf(request ,"GET /%s HTTP/1.1\r\nHost: %s\r\nUser-Agent: MY_SIM_GET\r\nConnection: close\r\n\r\n",buf2, hostname);
	write(sock, request, strlen(request));		

	char sock_buf[9999];
	char *out_buf;
	int bytes_received;

	bzero(sock_buf, 9999);
	// fill the buff 

	if (file == 0) { // if the file already exists
		while ((bytes_received = read(sock, sock_buf, 9999)) > 0) { // while we can still read data from the socket
			out_buf = sock_buf;  // chunk the first thing read from the socket and output it
			printf("%s\n", out_buf);
			bzero(sock_buf, 9999);
		}
	}
	else {	// we need to create the file
		output = fopen(fname, "w+"); // w+ to write a new file
		int if_flag = 0;
	
		while ((bytes_received = read(sock, sock_buf, 9999)) > 0) {
			if (if_flag == 0 ) { // only do this once so increment the flag after we are done. we want to ignore the '<'
				out_buf = strchr(sock_buf, '<');	
				if_flag = if_flag + 1;
			}
			else
				out_buf = sock_buf;

			fputs(out_buf, output);  // put the socket buff data into the file that we opened
			bzero(sock_buf, 9999);
		}
		fclose(output);
   }
 
   close(sock);
  	return 0;	
}

