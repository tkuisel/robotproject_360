// Clemson University CPSC 3600 Networking. Group project client. 
// Michael Smith <mjs3@clemson.edu>

#include <stdio.h>
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <netdb.h>      /* for getHostByName() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <stdint.h>		/* for standard datatypes */
#include <time.h>		/* for time functions */

#define PACKET_SIZE 300
#define HEADER_SIZE 28

/* Command definitions */

#define CONNECT_CMD 0
#define QUIT_CMD 255
#define IMAGE_CMD 2
#define GPS_CMD 4
#define DGPS_CMD 8
#define LASERS_CMD 16
#define MOVE_CMD 32
#define TURN_CMD 64
#define STOP_CMD 128

/* Commands can be OR together to create multiple commands in one packet 
   but, we dont need to OR them together. can just send each command seperately.

connect = 0000 0000
image   = 0000 0010
GPS     = 0000 0100
dGPS    = 0000 1000
lasers  = 0001 0000
move    = 0010 0000
turn    = 0100 0000
stop    = 1000 0000
quit    = 1111 1111

*/

/* Globals */

uint32_t respPass = 0;


/* protocol header data */
struct header
{
//	uint32_t data[7];
	uint32_t ID; 		// Protocol Identifier
	uint32_t pass;		// Password
	uint32_t request;	// Client Request
	uint32_t reqData;	// Request Data
	uint32_t offset;	// Byte Offset
	uint32_t totalSize;	// Total Size in Bytes
	uint32_t paysize;	// Payload Size*/
};

void firstConnectfunc(struct header *head, int sock, uint8_t packet[], struct sockaddr_in servAddr, struct sockaddr_in fromAddr ) // figure what extra arameters to send
{

	/* Still needs to clear out buffer space, attach header and send packet, then receive responce and parse password from received packet
	and then set password variable to what server's password is
	*/

	fprintf(stderr, "clearing header, it's size is: %d\n", HEADER_SIZE);

	fprintf(stderr, "clearing packet, it's size is: %d\n", PACKET_SIZE);

	memset(packet, 0, PACKET_SIZE); // zero out packet

	memset(head, 0, HEADER_SIZE); // clear out header
	//head->data[0] = 2;

	memcpy(packet, head, HEADER_SIZE); // copy header to packet

/*
	fprintf(stderr,"ID: %d\n", head->data[0]);
	fprintf(stderr,"pass: %d\n", head->data[1]);
	fprintf(stderr,"request: %d\n", head->data[2]);
	fprintf(stderr,"reqdata: %d\n", head->data[3]);
	fprintf(stderr,"offset: %d\n", head->data[4]);
	fprintf(stderr,"totalsize: %d\n", head->data[5]);
	fprintf(stderr,"paysize: %d\n", head->data[6]);
*/

	/* Send blank header, responce will contain server password */
	sendto(sock, packet, HEADER_SIZE, 0, (struct sockaddr *)&servAddr, sizeof(servAddr));

	fprintf(stderr, "Blank header sent\n");

	int fromSize = sizeof(fromAddr);



	recvfrom(sock, packet, PACKET_SIZE , 0, (struct sockaddr *)&fromAddr, (socklen_t *)&fromSize);

	fprintf(stderr, "password received!\n");

	/* copy header back into struct and retrive server password */

	memcpy(head, packet, sizeof(struct header));

	respPass = head->pass;

	fprintf(stderr, "password is: %d \n", head->pass);

	/* Ack with server password */
	sendto(sock, packet, HEADER_SIZE, 0, (struct sockaddr *)&servAddr, sizeof(servAddr));

	fprintf(stderr, "sent ack for first connect\n\n");
}


void sendCommand(struct header *head, int sock, uint8_t packet[], struct sockaddr_in servAddr, struct sockaddr_in fromAddr, uint32_t command)
{

	//fprintf(stderr, "sending %d command\n", command);	

	uint8_t *imgbuf; 								// pointer to malloced buffer for image
	uint32_t fileSize = 0; 							// total size of received file

	unsigned int addrsize = sizeof(servAddr); // for recvfrom

	uint8_t tries = 0;
	while(tries < 3) // set up 3 tries method
	{
		fprintf(stderr, "\n\n");
		fprintf(stderr, "sending %d command\n", command);

		memset(packet, 0, PACKET_SIZE);					// clear packet

		fprintf(stderr, "Packet cleared\n");		

		memset(head, 0, HEADER_SIZE);			// clear header

		fprintf(stderr, "header cleared\n");
		
		head->pass = respPass; 							// Set server password
		head->request = command; 						// set command
		if(command == TURN_CMD || command == MOVE_CMD)
		{
			head->reqData = 1; // if move or turn command, set the distance/unit
		}

		memcpy(packet, head, HEADER_SIZE);//sizeof(struct header)); 	// Copy header to packet
		fprintf(stderr, "header copied to packet\n");



		/* Send request to server */
		fprintf(stderr, "sending request\n");
		sendto(sock, packet, HEADER_SIZE, 0, (struct sockaddr *)&servAddr, sizeof(servAddr));
		/* receive ack */
		fprintf(stderr, "receiving ack and checking\n");

		if((recvfrom(sock, packet, PACKET_SIZE, 0, (struct sockaddr *)&servAddr, &addrsize)) <= 0)
		{
			if(tries == 3)
			{
				fprintf(stderr, "\tRetry failed!!!\n");
				exit(1);
			}
			//fprintf(stderr, "\tError receiving!!!\n");
			tries = 3;
			continue;
		}
		/* Check if ack is correct */

		fprintf(stderr, "ack received\n");
		memcpy(head, packet, HEADER_SIZE); 
		fprintf(stderr, "coping header from ack responce\n");									// retrive header
			if(head->pass != respPass || head->request != command)					// if invalid password or request is wrong, retry
			{
				if(tries == 3)
				{
					fprintf(stderr, "\tRetry failed!!!\n");
					exit(1);
				}
				fprintf(stderr, "\tAck error, password or responce do not match\n");	
				tries++;
				continue;
			}

		int recvsize = 0;

		/* first receive we must get the file metadata from the header */
		fprintf(stderr, "getting first data packet\n");
		if((recvsize = recvfrom(sock, packet, PACKET_SIZE, 0, (struct sockaddr *)&servAddr, &addrsize)) > 0)
		{
			memcpy(head, packet, HEADER_SIZE); 									// retrive header
			if(head->pass != respPass || head->request != command)														// if invalid password, ignore
			{
				if(tries == 3)
				{
					fprintf(stderr, "\tRetry failed!!!\n");
					exit(1);
				}
				fprintf(stderr, "\terror in first data packet, password or command do not match\n");
				tries++;
				continue;
			}
			if(command == IMAGE_CMD || command == LASERS_CMD) //  only image command sends large files
			{
				fileSize = head->totalSize;					  									// get total filesize to expect
				imgbuf = malloc(fileSize);					  									// malloc needed space for image buffer
				memcpy((imgbuf + head->offset), (packet)+HEADER_SIZE, head->paysize);	// put payload into buffer
			}
			else
			{
				fileSize = head->totalSize;					  									// get total filesize to expect
				imgbuf = malloc(fileSize);					  									// malloc needed space for image buffer
				memcpy((imgbuf), (packet)+HEADER_SIZE, head->totalSize);	// put payload into buffer
			}
		}
		else // if receive fails
		{		
				if(tries == 3)
				{
					fprintf(stderr, "Retry failed\n");
					exit(1);
				}
				fprintf(stderr, "\trecv error when trying to get first data packet \n");	
				tries++;
				continue;
		}	
		/* get rest of file */
		fprintf(stderr, "getting rest of file\n");
		if(command == IMAGE_CMD || command == LASERS_CMD)
		{
			while ((recvsize = recvfrom(sock, packet, PACKET_SIZE, 0, (struct sockaddr *)&servAddr, &addrsize)) > 0)
			{
				memcpy(head, packet, HEADER_SIZE); 									// get header
				memcpy((imgbuf + head->offset), (packet)+HEADER_SIZE, head->paysize);	// insert payload into buffer at offset
			}
		}
		fprintf(stderr, "writing responce data to file\n\n");
		char filename[32];
		if((command == MOVE_CMD) || (command == TURN_CMD) || (command == STOP_CMD))
		{
			fprintf(stderr, "Turn, move or stop command sent dont write anything to file\n\n");
			return;
		}
		else if(command == IMAGE_CMD)
		{
			fprintf(stderr, "writing image file\n");
			sprintf(filename , "%u image group 12.jpeg", (unsigned int)time(NULL)); // make filename for saving data
		}
		else if(command == LASERS_CMD)
		{
			fprintf(stderr, "writing lasers file\n");
			sprintf(filename , "%u lasers group 12.txt", (unsigned int)time(NULL)); // make filename for saving data
		}
		else
		{
			fprintf(stderr, "other sensor data\n");
			sprintf(filename , "%u group 12.txt", (unsigned int)time(NULL)); // make filename for saving data	
		}

		fprintf(stderr, "Writing final file now\n");
		FILE *output = fopen(filename, "wb");	// open file for writing
		fwrite(imgbuf, fileSize, 1, output);		// write file
		fclose(output);								// close file

		memset(head, 0, HEADER_SIZE);			// clear header
		
		head->pass = respPass; 							// Set server password
		head->request = command; 						// image command

		memcpy(packet, head, HEADER_SIZE); 	// Copy header to packet

		sendto(sock, packet, HEADER_SIZE, 0, (struct sockaddr *)&servAddr, sizeof(servAddr)); //  send final ack

		return;

	}

}



void getData(struct header *head, int sock, uint8_t packet[], struct sockaddr_in servAddr, struct sockaddr_in fromAddr)
{
	sendCommand(head, sock, packet, servAddr, fromAddr, GPS_CMD);
	//sendCommand(head, sock, packet, servAddr, fromAddr, DGPS_CMD);
	sendCommand(head, sock, packet, servAddr, fromAddr, LASERS_CMD);
}



int checkVars(unsigned short servPort, uint32_t numSides, uint32_t sideLength)
{
	if(servPort == 0 || numSides < 4 || sideLength == 0 || numSides > 8)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int main(int argc, char** argv)
{

 	int sock;                    	/* Socket descriptor */
    struct sockaddr_in servAddr;	/* Echo server address */
    struct sockaddr_in fromAddr;
    struct hostent *thehost;        /* Hostent from gethostbyname() */
    unsigned short servPort = 0;   	/* Echo server port */
    char *servIP;                   /* Server IP address (dotted quad) */

	struct header head;				/* protocol header struct */

	uint8_t packet[PACKET_SIZE]; 			/* payload plus header */
	
	uint32_t numSides = 0;			// for the shapes the robot must traverse
	uint32_t sideLength = 0;

	int i;

	for (i = 0; i < argc; i++) /* check arguements */
	{
		if(strcmp(argv[i], "-h") == 0)
		{
			servIP = argv[i+1];
		}

		if(strcmp(argv[i], "-p") == 0)
		{
			servPort = atoi(argv[i+1]);
		}
		if(strcmp(argv[i], "-n") == 0)
		{
			numSides = atoi(argv[i+1]);
		}
		if(strcmp(argv[i], "-l") == 0)
		{
			sideLength = atoi(argv[i+1]);
		}
	}

	if(checkVars(servPort, numSides, sideLength) == 0)
	{
		fprintf(stderr, "Error in client args, use: ./client -h <hostname-of-server> -p <port> -n <number of sides> -l <length of sides>\n");
		exit(1);
	}


        /* Create a datagram/UDP socket */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
    	fprintf(stderr, "Error creating socket\n");
    	exit(1);
    }


   	// Construct the server address structure */
   	memset(&servAddr, 0, sizeof(servAddr)); 		/* Zero out structure */
    servAddr.sin_family = AF_INET;                  /* Internet addr family */
    servAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    servAddr.sin_port   = htons(servPort);    		/* Server port */

   	/* If user gave a dotted decimal address, we need to resolve it  */
    if (servAddr.sin_addr.s_addr == -1) 
    {
        thehost = gethostbyname(servIP);
        servAddr.sin_addr.s_addr = *((unsigned long *) thehost->h_addr_list[0]);
    }


    /* Set Socket Tmeout options to 3 seconds for send and receive */
    struct timeval timeout;      
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;

    if (setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
    {
        fprintf(stderr,"setsockopt failed\n");
    }

    if (setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
    {
        fprintf(stderr,"setsockopt failed\n");
    }


    fprintf(stderr, "sockets made, sending first connect protocol\n\n");


    // first connection gets server password for session
    firstConnectfunc(&head, sock, packet, servAddr, fromAddr);

    	// Do N side shape first, then N-1 side shape
    	//sendCommand(&head, sock, packet, servAddr, fromAddr, IMAGE_CMD);
    	fprintf(stderr, "Making shape with %d sides", numSides);
			for(i = 0; i< numSides; i++)
    	{
    		/* move, sleep, stop -> turn, sleep, stop -> image & data -> repeat */
    		sendCommand(&head, sock, packet, servAddr, fromAddr, MOVE_CMD);
    		usleep((sideLength * 1000000));
    		sendCommand(&head, sock, packet, servAddr, fromAddr, STOP_CMD);

    		sendCommand(&head, sock, packet, servAddr, fromAddr, TURN_CMD);
    		usleep((int)((14/numSides) * 1000000));

    		sendCommand(&head, sock, packet, servAddr, fromAddr, STOP_CMD);

    		//sendCommand(&head, sock, packet, servAddr, fromAddr, IMAGE_CMD);
    		getData(&head, sock, packet, servAddr, fromAddr); // calls GPD, dGPD and lasers functions

    	}

    	for (i = 0; i < (numSides-1); i++)
    	{

    		/* Repeat for N-1 sides */

    		sendCommand(&head, sock, packet, servAddr, fromAddr, MOVE_CMD);
    		usleep((sideLength * 1000000));
    		sendCommand(&head, sock, packet, servAddr, fromAddr, STOP_CMD);

    		sendCommand(&head, sock, packet, servAddr, fromAddr, TURN_CMD);
    		usleep((int)((14/numSides) * 1000000));
    		sendCommand(&head, sock, packet, servAddr, fromAddr, STOP_CMD);

    	//	sendCommand(&head, sock, packet, servAddr, fromAddr, IMAGE_CMD);
    		getData(&head, sock, packet, servAddr, fromAddr); // calls GPD, dGPD and lasers functions
    	}

	fprintf(stderr, "Done.\n");    	


    	//robot will navigate N sided polygon, then N-1 sided polygon.


	return 0;
}




/* these are unmodified send to and receive from from UDP HW example.


	sendto(sock, packet, sizeof(packet), 0, (struct sockaddr *)
       &servAddr, sizeof(servAddr));

	// now receive responce fix this chunk below to make sure the correct responce is received

	printf("UDPEchoClient: And now wait for a response... \n");    
	fromSize = sizeof(fromAddr);

	// adjust this receivefrom to check responces
    if(respStringLen = recvfrom(sock, echoBuffer, ECHOMAX, 0, 
         (struct sockaddr *) &fromAddr, &fromSize))

    // adjust this to make sure the responce is from server
    if(echoServAddr.sin_addr.s_addr != fromAddr.sin_addr.s_addr)
    {
        fprintf(stderr,"Error: received a packet from unknown source \n");
    }
*/



    	
