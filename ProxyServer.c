#include "UDPProtocol.h"
#include <time.h> //for seeding random
#include <netinet/in.h>
#include <sys/socket.h>
#include <signal.h>
unsigned char **GPSData;
int GPSIndex = 0;
void serverCNTCCode(){
	FILE *f = fopen("GPS.txt", "w+");
	int i;
	char temp[180];
	fprintf(stderr,"Here is the GPS data:\n");
	for(i = 0; i < GPSIndex; i++){
		
    sprintf(temp, "%s", GPSData[i]);
		fwrite(temp, 1, sizeof(temp), f);
	}
	fclose(f);
	exit(0);
};
int randomizePassword(){
	int pass;
	pass = rand();
	fprintf(stderr,"Randomizing the password!\nThe new pass is %d\n", pass);
	return pass;
}
void DieWithError(char *);

buffer* createbuf(int size) {
    buffer* buf = (buffer*)malloc(sizeof(buffer));
    buf->size = size;
    buf->len = 0;
    buf->data = (unsigned char*)malloc(size * sizeof(unsigned char));
    return buf;
}
void resize(buffer* buf, int size) {
	if (size < buf->len)
	buf->len = size;
	unsigned char* temp = (unsigned char*)malloc(size * sizeof(unsigned char));
	memcpy(temp, buf->data, buf->len * sizeof(unsigned char));
	free(buf->data);
	buf->data = temp;
	buf->size = size;
}

void insert(buffer* b, unsigned char const* str, int len) {
  while ((b->len + len) > b->size) 
	  resize(b, b->size * 2);
  memcpy(b->data + b->len, str, len * sizeof(unsigned char));
  b->len += len;
}

int time_out(int sock,struct timeval timeout) {
    if (setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,(char*)&timeout,sizeof(timeout)) < 0) {
        fprintf(stderr,"ERROR: setsockopt(recv) failed\n");
        return 0;
    }
    if (setsockopt(sock,SOL_SOCKET,SO_SNDTIMEO,(char*)&timeout,sizeof(timeout)) < 0) {
        fprintf(stderr, "ERROR: setsockopt(send) failed\n");
        return 0;
    }
    return 1;
}

int connectrobot(char * hostname, int portNumber) {
    int sock;
	 struct sockaddr_in robotAddr;

    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP))<0)
        DieWithError("Socket() function has failed.");
  

	 memset(&robotAddr, 0, sizeof(robotAddr));
    robotAddr.sin_family = AF_INET;
    robotAddr.sin_addr.s_addr = inet_addr(hostname);
    robotAddr.sin_port = htons(portNumber);

    if (robotAddr.sin_addr.s_addr == -1) {
        struct hostent* host = gethostbyname(hostname);
        robotAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr_list[0]);    }
		
    if (connect(sock,(struct sockaddr*)&robotAddr,sizeof(robotAddr)) < 0)
        DieWithError("Connect() function has failed.");

    return sock;
}


buffer* performRequest(struct header *h, char *robotHost, char *robotID, char *robotNum){
	 int n;	
    unsigned char message[100000];  
    buffer* httpData = createbuf(100000);
  
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
  	 int robotsock;

    //HTTP request is created here
    memset(message, '\0', 1000);  
		
    switch (h->request) {
        case 2:
						snprintf((char*)message,100,"GET /snapshot?topic=/robot_%d/image?width=600?height=500 HTTP/1.1\r\n\r\n", 12);
            robotsock = connectrobot(robotHost, 8081);
            break;
        case 4:
            snprintf((char*)message,100,"GET /state?id=%s HTTP/1.1\r\n\r\n", "free1");
            robotsock = connectrobot(robotHost, 8082);
            break;
        case 8:
            snprintf((char*)message,100,"GET /state?id=%s HTTP/1.1\r\n\r\n", "free1");
            robotsock = connectrobot(robotHost, 8083);
            break;
        case 16:
            snprintf((char*)message,100,"GET /state?id=%s HTTP/1.1\r\n\r\n", "free1");
            robotsock = connectrobot(robotHost, 8084);
            break;
        case 32: 
						fprintf(stdout, "message: %s\n",(char*)message );
						fprintf(stdout, "name: %s\n","free1");
						fprintf(stdout, "host: %s\n",robotHost);
						fprintf(stdout, "port: %i\n",8082);
            snprintf((char*)message,100,"GET /twist?id=%s&lx=%d HTTP/1.1\r\n\r\n", "free1", 1);
            robotsock = connectrobot(robotHost, 8082);

            break;
        case 64:
            snprintf((char*)message,100,"GET /twist?id=%s&az=%d HTTP/1.1\r\n\r\n", "free1", 1);
            robotsock = connectrobot(robotHost,8082);
            break;
        case 128:
            snprintf((char*)message,100,"GET /twist?id=%s&lx=0 HTTP/1.1\r\n\r\n", "free1");
            robotsock = connectrobot(robotHost, 8082);
            break;
        default:
            fprintf(stderr, "ERROR: BAD CLIENT REQUEST\n");
            return NULL;
    }

    //Send the HTTP request that was just created
    fprintf(stdout, "\t\tRequest to server\n");
    time_out(robotsock,timeout);
    write(robotsock,(char*)message,strlen((char*)message));

    //Read HTTP message into a buffer
    fprintf(stdout, "\t\tReceiving reply from server\n");
    memset(message, '\0', 1000);
	 int attempts = 0;
    while (1) {
        n = read(robotsock,(char*)message, 1000);
        if (n == -1) {
            if (attempts > 2) {
                break;
            }
           	attempts ++;
            write(robotsock,(char*)message,strlen((char*)message));
            continue;
        } else if (n == 0) {
            break;
        }
        fprintf(stdout, "Received %d bytes\n", n);
        insert(httpData,(unsigned char*)message, n);
        memset(message, '\0', 1000);
    }
    fprintf(stdout, "Total bytes received: %d\n", httpData->len);
    fprintf(stdout, "Message:\n\n%s\n", httpData->data);

		if(h->request == 4){
			GPSData[GPSIndex++] = httpData->data;
		}
				
			/** Format robot Data into header */

    fprintf(stdout, "\nRequest command is complete.\n");
		return httpData;
}	



int main(int argc, char *argv[])
{
	signal(SIGINT, serverCNTCCode);
	//all the variables for the udpserver
	GPSData = malloc(1000);
	int sock;
	struct sockaddr_in UDPServAddr;
	struct sockaddr_in UDPClntAddr;
	
	unsigned int cliAddrLen;
	char buff[300];
	unsigned short UDPServPort;
	//variables for the robot information
	char *robotID;
	char *robotNum;
	char *robotHost;
	//variable to hold the password
	uint32_t password = 0;

	//seeding random for generating the password
	srand (time(NULL));

	//checks to see if there is an appropriate number of flags
	if(argc != 9)
		DieWithError("Not the correct number of flags");
	
	//loop to get all of the flags and their information
	int i;
	for(i = 1; i < argc; i++){
		if(strcmp(argv[i], "-h") == 0){
			i++;
			robotHost = argv[i];
		}
		else if(strcmp(argv[i], "-p") == 0){
			i++;
			UDPServPort = atoi(argv[i]);
		}
		else if(strcmp(argv[i], "-i") == 0){
			i++;
			robotID = argv[i];
		}
		else if(strcmp(argv[i], "-n") == 0){
			i++;
			robotNum = argv[i];
		}
		else
			DieWithError("Illegal flag");
	}
	//creates the socket
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		DieWithError("socket() failed");
	
	//sets the information for the server
	memset(&UDPServAddr, 0, sizeof(UDPServAddr));
	UDPServAddr.sin_family = AF_INET;
	UDPServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	UDPServAddr.sin_port = htons(UDPServPort);
	
	//binds to the port
	printf("ProxyServer: About to bind to port %d\n", UDPServPort);
	if(bind(sock, (struct sockaddr *) &UDPServAddr, sizeof(UDPServAddr)) < 0)
		DieWithError("bind() failed");
	
	//enternal loop to recieve connections
	for(;;){
		
		cliAddrLen = sizeof(UDPClntAddr);
		if((recvfrom(sock, buff, 300, 0,
			(struct sockaddr *) &UDPClntAddr, &cliAddrLen)) < 0)
			DieWithError("recvfrom() failed");

		printf("Handling client %s\n", inet_ntoa(UDPClntAddr.sin_addr));
		//if it the first time being used
		if(password == 0){
			fprintf(stderr,"First time accessing\n");
			password = randomizePassword();
			struct header *h = malloc(sizeof(struct header));
			h->ID = password;
			h->pass = password;
			h->request = password;
			h->reqData = password;
			h->offset = password;
			h->totalSize = password;
			h->Paysize = password;
			char *buff = malloc(300);
			memcpy(buff, h, 28);
			//send back the password
			sendto(sock, buff, sizeof(h), 0, (struct sockaddr *) &UDPClntAddr, sizeof(&UDPClntAddr)); 
			//send back the password
			if((recvfrom(sock, buff, 272, 0, (struct sockaddr *) &UDPClntAddr, &cliAddrLen)) < 0)
				DieWithError("recv failed");			
		}
		else {
			//check to see if the password matches if not tell that it is being used
			struct header *h = malloc(sizeof(struct header));
			memcpy(h, buff, sizeof(struct header));
			if(h->ID == 12){
				fprintf(stderr,"Our client reconginized using our protocol!\n");
			}
			buffer* httpData = performRequest(h, robotHost, robotID, robotNum);
			if(sizeof(httpData->data) > 272){
				struct header *head = malloc(sizeof(struct header));
				char *temp = malloc(300);
				head->pass = password;
				head->totalSize = sizeof(buff);
				int i;
				while(i< head->totalSize){
					head->offset = i;
					if((head->totalSize / i) != 0){
						head->Paysize = 272;
						memcpy(temp, head, sizeof(struct header));
						memcpy(temp + sizeof(struct header), buff + i, 272);
						i *= 272;
						if(sendto(sock, temp, sizeof(temp), 0, (struct sockaddr *) &UDPClntAddr, sizeof(&UDPClntAddr)) != sizeof(temp));
							DieWithError("sendto() sent a different number of bytes than expected");
						free(temp);
					}
				}
			}
			else{
				struct header *head = malloc(sizeof(struct header));
				head->pass = password;
				head->offset = 0;
				head->totalSize = httpData->len;
				head->Paysize = httpData->len;
				char *temp = malloc(300);
				memcpy(temp, head, sizeof(struct header));
				memcpy(temp + sizeof(struct header), httpData->data, head->Paysize);
				sendto(sock, temp, 300, 0, (struct sockaddr *) &UDPClntAddr, sizeof(&UDPClntAddr));
			}
		bzero(buff, sizeof(buff));
		}
	}
}
